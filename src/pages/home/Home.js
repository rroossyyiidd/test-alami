import '../../App.css';
import Wave from "../../components/wave";
import Navbar from "../../components/navbar";
import React from "react";
import IconCommand from '../../assets/icons/command.png'
import IconCube from '../../assets/icons/cube.png'
import IconLike from '../../assets/icons/like.png'
import IconQuil from '../../assets/icons/quil.png'
//css
import './style.css';

function Home() {
  return (
    <div className="App">
      <Navbar/>
      <div className="App-header">
        {/* hero image */}
        <div className="pl-5 pr-5">
          <div style={{
            backgroundColor: '#2D232B',
            opacity: 0.7,
            width: '100%',
          }}>
            <p className="title-text">
              Catering should be an experience
            </p>
            <p className="title-text">
              We use only the finest and freshest ingredients
            </p>
            <p
              style={{
                color: '#fff',
                fontSize: 22,
                marginTop: -5,
                fontFamily: "Cabin",
              }}>
              At Sway catering we know that food is an important part of life. If the meal is not perfect, your event cannot be perfect.
            </p>
          </div>
        </div>
      </div>
      {/* wave */}
      <Wave/>
      {/* content */}
      <div style={{height: 250}} className="bg-white mt-0 pt-lg-5">
        <div className="pt-lx bg-white">
          <h3>Catering services in New York</h3>
          <h3>We specialize in corporate and private events</h3>
          <p className="subtitle-content">with 20 years of experience, we promise you the freshest, local ingredients, hand-crafted cooking sptinkled with our unique whimsical elegance and exceptional service.</p>
        </div>
      </div>
      {/* portofolio */}
      <div className="bg-white mt-0 d-flex w-100 footer-porto">
        <div className="bg-portofolio p-4">
          <h3>My Portofolio</h3>
        </div>
        <div className="bg-porto-center p-4">
          <div className="box-porto-center">
            <img src={IconCube} className="icon-porto" alt="icon portofolio"/>
            <h4 className="mt-2 title-box-porto">Design</h4>
            <p className="service-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div className="box-porto-center">
            <img src={IconCommand} className="icon-porto" alt="icon portofolio"/>
            <h4 className="mt-2 title-box-porto">Develop</h4>
            <p className="service-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div className="box-porto-center">
            <img src={IconQuil} className="icon-porto" alt="icon portofolio"/>
            <h4 className="mt-2 title-box-porto">Write</h4>
            <p className="service-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div className="box-porto-center">
            <img src={IconLike} className="icon-porto" alt="icon portofolio"/>
            <h4 className="mt-2 title-box-porto">Promote</h4>
            <p className="service-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
        </div>
        <div className="bg-porto-right p-4">
          <h4 style={{color: '#FF566A'}}>Services</h4>
          <p className="hightlight-service-text">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
          </p>
          <p className="service-text">
            It has survived not only five centuries, but also the leap into electronic typesetting, 
            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software 
            like Aldus PageMaker including versions of Lorem Ipsum.
          </p>  
          <p className="service-text">
            It has survived not only five centuries, but also the leap into electronic typesetting, 
            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software 
            like Aldus PageMaker including versions of Lorem Ipsum.
          </p>
        </div>
      </div>
    </div>
  );
}

export default Home;
