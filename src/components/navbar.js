import { Navbar, Nav, Button, FormControl, InputGroup } from 'react-bootstrap';

export default function NavbarComponent() {
  return (
    <>
      <Navbar bg="light" expand="lg" variant="light" className="fixed-top">
        <Navbar.Brand href="/"/>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">Demo</Nav.Link>
            <Nav.Link href="#">Pages</Nav.Link>
            <Nav.Link href="##">Portofolio</Nav.Link>
          </Nav>
          <div>
            <InputGroup>
              <FormControl aria-describedby="basic-addon1" placeholder="Search" />
              <InputGroup.Prepend>
                <Button variant="outline-secondary">Search</Button>
              </InputGroup.Prepend>
            </InputGroup>
          </div>
        </Navbar.Collapse>
      </Navbar>
    </>
  )
}
